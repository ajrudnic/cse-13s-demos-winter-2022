#include <stdio.h>
#include <stdlib.h>

typedef struct {
  int studentid;
  int assignment_num;
  int grade;
} StudentGrade;

void give_student_zero(StudentGrade *studentgrade) {
  studentgrade->grade = 0;
}

int main(void) {
  StudentGrade studentgrades[2];

  StudentGrade a7 = {100, 7, 5};
  StudentGrade a8 = {100, 7, 5};
  studentgrades[0] = a7;
  studentgrades[1] = a8;

  StudentGrade* p = studentgrades;
  give_student_zero(p);

  p++;
  give_student_zero(p);

  // give_student_zero(p);

  printf("a7 grade: %d\n", studentgrades[0].grade);
  printf("a8 grade: %d\n", studentgrades[1].grade);

  return 0;
}
