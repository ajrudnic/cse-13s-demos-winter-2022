#include <stdio.h>

int add_three(int x) {
  return x + 3;
}

int return_zero(int x) {
  return 0;
}

void map(int *inputs, int *outputs, int size, int (*funk)(int)) {
  for (int i = 0; i < size; i++) {
    outputs[i] = funk(inputs[i]);
  }
}

int main(void) {
  int nums[] = {13, 23, 420, 7, 17};
  int outputs[5];

  int i;
  int *ptr_to_i;

  ptr_to_i = &i;

  *ptr_to_i = 15;
  i = 15;

  map(nums, outputs, 5, add_three);
  // map(nums, outputs, 5, return_zero);

  for (int i = 0; i < 5; i++){
    printf("outputs[%d] = %d\n", i, outputs[i]);
  }

  return 0;
}
