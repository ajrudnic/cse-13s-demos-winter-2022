#include <stdio.h>

int main(void) {

  int secret_number = 705;

  int *a;
  a = &secret_number;

  printf("old value of the secret number: %d\n", secret_number);
  printf("memory address: %p\n", a);

  *a = 1337;

  printf("memory address: %p\n", a);
  printf("new value of the secret number: %d\n", secret_number);


  return 0;
}
