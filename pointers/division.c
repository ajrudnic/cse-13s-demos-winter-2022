#include <stdio.h>


void division(int numerator, int denominator, int *quotient, int *remainder) {
  int amount_left = numerator;

  int quotient_out = 0;
  while((amount_left - denominator) >= 0) {
    amount_left -= denominator;
    quotient_out++;
  }
  *quotient = quotient_out;
  *remainder = amount_left;
}

int main(void) {
  int quotient;
  int remainder;

  division(29, 30, &quotient, &remainder);
  printf("quotient: %d, remainder %d\n", quotient, remainder);

  return 0;
}
