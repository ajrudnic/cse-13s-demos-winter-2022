#include <stdio.h>

int main(void) {
  // count from 1 to 10 and print out the numbers.
  // for loop
  for(int i = 1; i <= 10; i++) {
    printf("for loop i is now: %d\n", i);
  }

  // while loop
  int i = 100;
  while(i <= 10) {
    printf("while loop i is now: %d\n", i);
    i++;
  }

  // do-while
  i = 100;
  do {
    printf("do loop i is now: %d\n", i);
    i++;
  } while(i <= 10);


  return 0;
}
