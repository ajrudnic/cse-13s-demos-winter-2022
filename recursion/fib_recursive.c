#include <stdio.h>
#include <stdlib.h>

#define TABLE_SIZE 1024

long long fib(int k, long long *memo_table, int len) {
  if (k == 0 || k == 1) {
    return k;
  }

  if (k >= len) {
    printf("sorry!\n");
    return 0;
  }

  if (memo_table[k]) {
    return memo_table[k];
  }
  long long answer = fib(k-1, memo_table, len) + fib(k-2, memo_table, len);
  memo_table[k] = answer;

  return answer;
}

int main(int argc, char **argv) {
  int x;

  long long memo_table[TABLE_SIZE];

  if (argc < 2) {
    x = 10;
  } else {
    x = strtol(argv[1], NULL, 10);
  }
  printf("%lld\n", fib(x, memo_table, TABLE_SIZE));

  return 0;
}
