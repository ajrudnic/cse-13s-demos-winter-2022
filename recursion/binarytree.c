#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct TreeNode TreeNode;

struct TreeNode {
  int data;
  TreeNode *left;
  TreeNode *right;
};

TreeNode *create_treenode(int item, TreeNode* left, TreeNode* right) {
  TreeNode *newnode = (TreeNode *)calloc(1, sizeof(TreeNode));

  newnode->data = item;

  newnode->left = left;
  newnode->right = right;
  return newnode;
}

bool search_tree(TreeNode *tree, int value) {
  if (tree == NULL) {
    return false;
  }
  if (tree->data == value) {
    return true;
  } else if(search_tree(tree->left, value)) {
    return true;
  } else if(search_tree(tree->right, value)) {
    return true;
  }
  return false;
}

int main(void) {

  TreeNode *a = create_treenode(15, NULL, NULL);
  TreeNode *b = create_treenode(18, NULL, NULL);
  TreeNode *top = create_treenode(420, a, b);

  bool found = search_tree(top, 420);
  printf("did I find it? %d\n", found);

  found = search_tree(top, 421);
  printf("did I find it? %d\n", found);

  return 0;
}
