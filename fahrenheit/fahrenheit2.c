#include <stdio.h>

int main(void) {
  float fahrenheit;
  float celsius;

  /* fahrenheit = 58; */
  int start = 0;
  int end = 300;


  fahrenheit = start;
  while (fahrenheit <= end) {
    celsius = ((fahrenheit - 32) / 9) * 5;
    printf("fahrenheit: %3.0f celsius: %6.1f\n", fahrenheit, celsius);
    fahrenheit += 10;
  }

  return 0;
}
