#include <stdio.h>

int main(void) {
  float celsius;

  int end = 300;
  for (float fahrenheit = 0; fahrenheit <= end; fahrenheit += 10) {
    celsius = ((fahrenheit - 32) / 9) * 5;
    printf("fahrenheit: %3.0f celsius: %6.1f\n", fahrenheit, celsius);
  }
  return 0;
}
