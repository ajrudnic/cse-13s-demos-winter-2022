#include <stdio.h>
#include <limits.h>

int minIndex(int *arr, int beginning, int last) {
  int smallest_index = beginning;
  int smallest_val = INT_MAX;

  for(int i = beginning; i < last; i++) {
    if (arr[i] < smallest_val) {
      smallest_val = arr[i];
      smallest_index = i;
    }
  }
  return smallest_index;
}

void minSort(int *arr, int len) {
  for (int beginning = 0; beginning < len; beginning++) {
    // find the smallest one
    int min_index = minIndex(arr, beginning, len);
    // swap indices of the first one and the smallest one
    if (min_index != beginning) {
      int tmp = arr[beginning];
      arr[beginning] = arr[min_index];
      arr[min_index] = tmp;
    }
    // keep going until the first one is == (len - 1)
  }
}

int main(void) {
  // int size = 5;
  // int nums[] = {420, 15, 0, 100, 42};
  int size = 10;
  int nums[] = {420, 15, 0, 100, 42,
                420, 15, 0, 100, 42};

  printf("%d\n", minIndex(nums, 0 , size));

  printf("BEFORE!\n");
  for (int i = 0; i < size; i++) {
    printf("nums[%d] = %d\n", i, nums[i]);
  }

  minSort(nums, size);

  printf("AFTER!\n");
  for (int i = 0; i < size; i++) {
    printf("nums[%d] = %d\n", i, nums[i]);
  }

  return 0;
}
