#include <stdio.h>

// has to take a positive number.
int sum_digits(int num) {
  printf("I got: %d\n", num);
  if (num <= 0) {
    return 0;
  }
  return (num % 10) + sum_digits(num / 10);
}

int main(void) {
  int random_number = 1111420;

  int summed = sum_digits(random_number);
  printf("%d\n", summed);
}
