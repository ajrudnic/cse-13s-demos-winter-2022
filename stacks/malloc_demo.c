#include <stdio.h>
#include <stdlib.h>


int main(void) {
  const int num_ints = 1000;
  int *nums;

  // nums = calloc(num_ints, sizeof(int));
  nums = (int *)malloc(4000);
  if (nums == NULL) {
    printf("allocation failed\n");
    return 0;
  }

  nums[0] = 21;

  printf("here is nums[0]: %d\n", nums[0]);
  free(nums);

  return 0;
}
