#include <stdio.h>

int main(void) {

  int bitwise_and = 8 & 7;
  int bitwise_or = 8 | 7;

  unsigned char c;

  c = ~(255);
  printf("%d\n", bitwise_and);
  printf("%d\n", bitwise_or);

  printf("%ud\n", (unsigned int)c);

  return 0;
}
