#include <stdio.h>

void print_binary(char c) {
  int val;

  for(int shift=7; shift >= 0; shift--) {
    val = (c & (1 << shift)) >> shift;
    printf("%d", val);
  }
  printf("\n");
}

int main(void) {
  char a = 'a';
  char b = 'b';

  print_binary(a);
  print_binary(b);

  return 0;
}
