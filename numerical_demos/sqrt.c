#include "sqrt.h"
#include <math.h>

#define EPSILON 0.000001

double my_sqrt(double x) {
  double guess = 1.0;
  double nextguess = 0.0;
  double diff = 100;

  while (diff > EPSILON) {
    nextguess = (guess + (x / guess)) / 2.0;
    diff = fabs(nextguess - guess);
    guess = nextguess;
  }
  return nextguess;
}

